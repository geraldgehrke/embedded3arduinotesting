// includes
#include<TimedAction.h>



// begin globals

  // threads
  thread1 = TimedAction(500, lightOn);
  thread2 = TimedAction(700, lightOff);

// end globals


// begin helper functions

  void lightOn(){
    digitalWrite(1, HIGH);
  }
  void lightOff(){
    digitalWrite(1, LOW);
  }

// end helper functions


void setup() {
  
}

void loop() {
  thread1.check();
  thread2.check();
}
